﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using System.Windows;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Popups;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=402352&clcid=0x409

namespace Assessment_2___9998591
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class MainPage : Page
    {
        public int option1 = 0;
        public int option2 = 0;        
        public string msgtxt = "";
        public string From1 = "";
        public MainPage()
        {
            this.InitializeComponent();
        }

     

        static string SubjectChoice(int option2)
        {
            var SecondChoice = "";

            switch (option2)
            {
                case 0:
                    SecondChoice = "Low Priority";
                    break;
                case 1:
                    SecondChoice = "Medium Priority";
                    break;
                case 2:
                    SecondChoice = "High Priority";
                    break;
            }
            return SecondChoice;
        }
        private async void submitbtn_Click(object sender, RoutedEventArgs e)
        {
            From1 = FromTextBox.Text;
            msgtxt = msgtxtBox.Text;
            option1 = totextbox.SelectedIndex;
            option2 = SubjectBox.SelectedIndex;

            MessageDialog CheckMessage = new MessageDialog($"From: {From1}\nSubject: {SubjectChoice(option2)}\nMessage:\n\n{msgtxt}");
            CheckMessage.Title = "Are you sure you want to send this message?";

            CheckMessage.Commands.Add(new Windows.UI.Popups.UICommand("Send") { Id = 0 });
            CheckMessage.Commands.Add(new Windows.UI.Popups.UICommand("Cancel") { Id = 1 });
            CheckMessage.Commands.Add(new Windows.UI.Popups.UICommand("Reset") { Id = 2 });

            CheckMessage.DefaultCommandIndex = 0;
            CheckMessage.CancelCommandIndex = 1;

            var result = await CheckMessage.ShowAsync();

            if ((int)result.Id == 0)
            {
                Application.Current.Exit();
            }
            else if ((int)result.Id == 2)
            {
                totextbox.SelectedIndex = -1;
                FromTextBox.Text = String.Empty;
                SubjectBox.SelectedIndex = -1;
                msgtxtBox.Text = string.Empty;
            }
        }

    }
}